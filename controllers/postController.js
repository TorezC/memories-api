const  mongoose = require('mongoose');
const postModel = require('../models/post.model');

exports.getPosts = async (req, res) =>{
    try {
        const posts = await postModel.find();
        console.log(posts);
        res.status(200).json(posts);
    } catch (error) {
        res.status(404).json({ message: error.message});
    }
}

exports.createPost = async (req, res) => {
    const post = req.body;
    const newPost = new postModel(post);
    try {
        await newPost.save();
        console.log(newPost);
        res.status(201).json(newPost);
    } catch (error) {
        res.status(409).json({ message: error.message});
    }
}

exports.updatePost = async (req, res) => {
    const { id: _id} = req.params;
    const post = req.body;

    if(!mongoose.Types.ObjectId.isValid(_id))
        return res.status(404).json({message: 'No post with that id'});

    const updatedPost = await postModel.findByIdAndUpdate(_id, { ...post, _id}, { new: true});
    res.json(updatedPost);

}

exports.deletePost = async (req, res) => {
    const { id } = req.params;

    if(!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).json({message: 'No post with that id'});

    await postModel.findByIdAndDelete(id);
    res.json({ message: 'Post deleted successfully'});

}

exports.likePost = async (req, res) => {
    const {id} = req.params;
    if(!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).json({message: 'No post with that id'});

    const post = await postModel.findById(id);
    const updatedlikePost = await postModel.findByIdAndUpdate(id, { likeCount: post.likeCount + 1}, {new: true});

    res.json(updatedlikePost);

}