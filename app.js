const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
global.appName = 'Memories API';

const port = process.env.PORT || 5000;
// const MONGODB_ATLAS_URL = ;

const postRouter = require('./routes/post');

const app = express();
app.listen(port, () => { console.log(`[${appName}]: http://localhost:${port}`) });
app.use(cors());

app.use(bodyParser.json({limit: '30mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '30mb', extended: true}));

app.use('/api/posts', postRouter);

mongoose.connect(process.env.MONGODB_ATLAS_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then(() => { console.log(`Connected correctly to MongoDB server for ${appName}..`); })
    .catch((error) => { console.error(`Connection error to MongoDB Server. [Issue]: \n ${error}`); });
// mongoose.set('useFindandModify', false);



